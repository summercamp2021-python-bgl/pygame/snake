from sizeConversion import IMTupel, CM

WINDOW_SIZE: IMTupel = IMTupel(CM(30), CM(20))
WINDOW_TITLE: str = "Title of the Game"

MAX_FPS = 120
