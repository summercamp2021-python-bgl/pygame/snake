from __future__ import annotations

import abc
from typing import Any, Optional
from timing import Timing
import pygame as game
import pygame.event as event


class Scene(metaclass=abc.ABCMeta):
    _sceneManager: SceneManager

    def setSceneManager(self, sceneManager: SceneManager) -> None:
        self._sceneManager = sceneManager
        return

    @abc.abstractmethod
    def init(self, surface: game.Surface) -> None: return

    def onOpen(self, surface: game.Surface, arg: Any) -> None: return

    def logic(self, surface: game.Surface, dT: float) -> bool: return True

    @abc.abstractmethod
    def render(self, surface: game.Surface) -> None: return

    @abc.abstractmethod
    def handleEvent(self, event: event.Event) -> None: return

    def onBackground(self, surface: game.Surface, arg: Any) -> None: return

    def onClose(self, surface: game.Surface, arg: Any) -> None: return


class SceneManager:
    _stack: list[str]
    _scenes: dict[str, tuple[Scene, bool]] = {}
    _surface: Optional[game.Surface] = None
    _timing: Timing = Timing()

    def __init__(self, defaultStack: list[str]) -> None:
        self._stack = defaultStack
        pass

    @property
    def timing(self) -> Timing:
        return self._timing

    def register(self, name: str, scene: Scene) -> None:
        scene.setSceneManager(self)
        self._scenes[name] = (scene, False)
        pass

    def push(self, name: str, data: Any = None) -> None:
        self._checkHasSurface()
        self._currentScene.onBackground(self._surface, data)
        self._stack.append(name)
        self._initScene(self._surface)
        self._currentScene.onOpen(self._surface, data)
        pass

    def pushAndReplace(self, name: str, data: Any = None) -> None:
        self._checkHasSurface()
        self._currentScene.onBackground(self._surface, data)
        self._currentScene.onClose(self._surface, data)
        del self._stack[-1]
        self._stack.append(name)
        self._initScene(self._surface)
        self._currentScene.onOpen(self._surface, data)
        pass

    def pop(self, data: Any = None) -> None:
        self._checkHasSurface()
        self._currentScene.onBackground(self._surface, data)
        self._currentScene.onClose(self._surface, data)
        del self._stack[-1]
        self._initScene(self._surface)
        self._currentScene.onOpen(self._surface, data)
        pass

    def renderScene(self, name: str) -> None:
        if name not in self._scenes:
            raise KeyError(f"Unknown scene \"{name}\"")
        self._checkHasSurface()
        assert self._surface is not None
        self._initScene(self._surface, name)
        self._scenes[name][0].render(self._surface)
        pass

    @property
    def _currentScene(self):
        return self._scenes[self._stack[-1]][0]

    def _checkHasSurface(self):
        if self._surface is None:
            raise RuntimeError("SceneManager: no surface found")
        pass

    def _checkSel(self):
        if len(self._scenes) == 0:
            raise RuntimeError("Rendered before registering any Scenes")
        elif self._stack[-1] not in self._scenes:
            raise KeyError(f"Unknown scene \"{self._stack[-1]}\"")

    def _initScene(self, surface: game.Surface = None,
                   name: str = None) -> None:
        self._checkSel()
        if surface is None:
            self._checkHasSurface()
            surface = self._surface
        assert surface is not None
        if not self._scenes[name if name is not None else self._stack[-1]][1]:
            self._scenes[
                name if name is not None else self._stack[-1]
            ][0].init(surface)
        pass

    def _initDefaultScene(self, surface: game.Surface) -> None:
        self._initScene(surface)
        self._currentScene.onOpen(surface, None)
        pass

    def init(self, surface: game.Surface) -> None:
        self._checkSel()
        self._surface = surface
        self._initDefaultScene(surface)
        pass

    def render(self, surface: game.Surface, dT: float) -> None:
        self._checkSel()
        if self._currentScene.logic(surface, dT):
            self._currentScene.render(surface)
        self._timing.update(int(dT * 1000))
        pass

    def handleEvent(self, event: event.Event) -> None:
        self._checkSel()
        self._currentScene.handleEvent(event)
        pass
