from typing import Any

import pygame as game
import pygame.event as event
from pygame.surface import Surface

from sizeConversion import CM
from scene import Scene
import scenes
import fonts

COLOR = (0x00, 0x3F, 0x00)
MARGIN = CM(3)

sceneName = "finish"


class FinishScene(Scene):
    _type: str = ""

    def __init__(self) -> None:
        super().__init__()

    def init(self, surface: game.Surface) -> None:
        return super().init(surface)

    def onOpen(self, surface: game.Surface, arg: Any) -> None:
        if arg is None or "type" not in arg:
            arg = {"type": "pause"}
        self._type = arg["type"]
        return super().onOpen(surface, arg)

    def logic(self, surface: game.Surface, dT: float) -> bool:
        return super().logic(surface, dT)

    def render(self, surface: game.Surface) -> None:
        size_x, size_y = surface.get_size()
        self._sceneManager.renderScene(scenes.snakeSceneName)
        newSurface = game.Surface(
            (size_x - MARGIN.toPx() * 2, size_y - MARGIN.toPx() * 2))
        surface_width, surface_height = newSurface.get_size()
        newSurface.set_alpha(0xAF)
        newSurface.fill(COLOR)
        if fonts.default_56 is not None:
            text: Surface = fonts.default_56.render(
                "Pause" if self._type == "pause" else
                (
                    "You failed!" if self._type == "failed"
                    else ""
                ),
                True,
                (0xFF, 0x00, 0x00)
            )

        width, height = text.get_size()
        newSurface.blit(text, ((surface_width - width) //
                        2, (surface_height - height) // 2))
        surface.blit(newSurface, (MARGIN.toPx(), MARGIN.toPx()))
        return super().render(surface)

    def handleEvent(self, event: event.Event) -> None:
        if event.type == game.KEYDOWN:
            if event.key == game.K_p and self._type == "pause":
                self._sceneManager.pop()
        return super().handleEvent(event)

    def onBackground(self, surface: game.Surface, arg: Any) -> None:
        return super().onBackground(surface, arg)

    def onClose(self, surface: game.Surface, arg: Any) -> None:
        return super().onClose(surface, arg)
