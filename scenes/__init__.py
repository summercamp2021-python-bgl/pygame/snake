from .home import HomeScene, sceneName as homeSceneName
from .snake import SnakeScene, sceneName as snakeSceneName
from .finish import FinishScene, sceneName as finishSceneName


homeSceneName
HomeScene

snakeSceneName
SnakeScene

finishSceneName
FinishScene
