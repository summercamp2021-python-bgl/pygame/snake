import scenes
from typing import Any, Optional
from pygame.rect import Rect
from sizeConversion import CM
from constants import WINDOW_SIZE
from scene import Scene
from enum import Enum
import fonts

import random

import pygame as game
import pygame.event as event
import pygame.draw as draw

BACKGROUND_COLOR = (0xFF, 0xFF, 0xFF)
RASTER_SIZE = CM(0.5)
SPEED = 20  # movement per second
TEXT_MARGIN = 20


class SnakeDir(Enum):
    LEFT = 0
    RIGHT = 1
    UP = 2
    DOWN = 3


sceneName = "snake"


class SnakeScene(Scene):
    _snakeDir: SnakeDir = SnakeDir.DOWN
    _nextSnakeDir: Optional[SnakeDir] = None
    _snakeRasterSize: tuple[int, int] = ((WINDOW_SIZE.x.toPx(
    ) // RASTER_SIZE.toPx()), (WINDOW_SIZE.y.toPx() // RASTER_SIZE.toPx()))
    _snakeRasterVal: tuple[int, int] = (WINDOW_SIZE.x.toPx(
    ) // _snakeRasterSize[0], WINDOW_SIZE.y.toPx() // _snakeRasterSize[1])
    _snake: list[tuple[int, int]] = \
        [(_snakeRasterSize[0] // 2 + 2, _snakeRasterSize[1] // 2),
         (_snakeRasterSize[0] // 2 +
          1, _snakeRasterSize[1] // 2),
         (_snakeRasterSize[0] // 2, _snakeRasterSize[1] // 2)]

    _fruit: Optional[tuple[int, int]] = None
    _everySecondInterval = None
    _points: int = 0

    def __init__(self) -> None:
        super().__init__()

    def _getNextSnakePos(self) -> tuple[int, int]:
        nextSnakePos = self._snake[-1]
        if self._snakeDir == SnakeDir.DOWN:
            nextSnakePos = (nextSnakePos[0], nextSnakePos[1] + 1)
            pass
        elif self._snakeDir == SnakeDir.UP:
            nextSnakePos = (nextSnakePos[0], nextSnakePos[1] - 1)
            pass
        elif self._snakeDir == SnakeDir.LEFT:
            nextSnakePos = (nextSnakePos[0] - 1, nextSnakePos[1])
            pass
        else:
            nextSnakePos = (nextSnakePos[0] + 1, nextSnakePos[1])
            pass

        if nextSnakePos[0] >= self._snakeRasterSize[0]:
            nextSnakePos = (0, nextSnakePos[1])
            pass
        elif nextSnakePos[0] < 0:
            nextSnakePos = (self._snakeRasterSize[0] - 1, nextSnakePos[1])
            pass

        if nextSnakePos[1] >= self._snakeRasterSize[1]:
            nextSnakePos = (nextSnakePos[0], 0)
            pass
        elif nextSnakePos[1] < 0:
            nextSnakePos = (nextSnakePos[0], self._snakeRasterSize[1] - 1)
            pass

        return nextSnakePos

    def _updateSnake(self, addEl=False) -> None:
        self._snake.append(self._getNextSnakePos())
        if not addEl:
            del self._snake[0]
        pass

    def _failed(self) -> None:
        self._sceneManager.push(scenes.finishSceneName, {"type": "failed"})
        pass

    def init(self, surface: game.Surface) -> None:
        return super().init(surface)

    def onOpen(self, surface: game.Surface, arg: Any) -> None:
        self._everySecondInterval = self._sceneManager.timing.setInterval(
            int(1000 / SPEED), self._snakeUpdate)
        return super().onOpen(surface, arg)

    def logic(self, surface: game.Surface, dT: float) -> bool:
        if self._fruit is None:
            self._fruit = (
                random.randint(0, self._snakeRasterSize[0] - 1),
                random.randint(0, self._snakeRasterSize[1] - 1)
            )
        return super().logic(surface, dT)

    def _snakeUpdate(self):
        addEl = False

        if self._nextSnakeDir is not None:
            self._snakeDir = self._nextSnakeDir

        nextSnakePos = self._getNextSnakePos()

        if nextSnakePos in self._snake[1:]:
            return self._failed()

        if nextSnakePos == self._fruit:
            addEl = True
            self._fruit = None
            self._points += 1

        self._updateSnake(addEl)
        pass

    def render(self, surface: game.Surface) -> None:
        surface.fill(BACKGROUND_COLOR)
        surface_width, surface_height = surface.get_size()

        if self._fruit is not None:
            fruit_x, fruit_y = self._fruit
            draw.ellipse(surface, (0xFF, 0x00, 0x00), Rect(
                fruit_x *
                self._snakeRasterVal[0], fruit_y * self._snakeRasterVal[1],
                self._snakeRasterVal[0], self._snakeRasterVal[1])
            )

        for x, y in self._snake[:-1]:
            draw.ellipse(surface, (0x00, 0xFF, 0x00), Rect(
                x * self._snakeRasterVal[0], y * self._snakeRasterVal[1],
                self._snakeRasterVal[0], self._snakeRasterVal[1])
            )
        head_x, head_y = self._snake[-1]
        draw.ellipse(surface, (0x00, 0x00, 0xFF), Rect(
            head_x * self._snakeRasterVal[0], head_y * self._snakeRasterVal[1],
            self._snakeRasterVal[0], self._snakeRasterVal[1])
        )

        if fonts.default_32 is not None:
            text = fonts.default_32.render(
                str(self._points),
                True,
                (0xAF, 0x00, 0x00)
            )
            width, height = text.get_size()
            surface.blit(text, (surface_width - width -
                         TEXT_MARGIN, TEXT_MARGIN))
            pass

        return super().render(surface)

    def handleEvent(self, event: event.Event) -> None:
        if event.type == game.KEYDOWN:
            if (event.key == game.K_w or event.key == game.K_UP) and \
                    self._snakeDir != SnakeDir.DOWN:
                self._nextSnakeDir = SnakeDir.UP
                pass
            elif (event.key == game.K_s or event.key == game.K_DOWN) and \
                    self._snakeDir != SnakeDir.UP:
                self._nextSnakeDir = SnakeDir.DOWN
                pass
            elif (event.key == game.K_a or event.key == game.K_LEFT) and \
                    self._snakeDir != SnakeDir.RIGHT:
                self._nextSnakeDir = SnakeDir.LEFT
                pass
            elif (event.key == game.K_d or event.key == game.K_RIGHT) and \
                    self._snakeDir != SnakeDir.LEFT:
                self._nextSnakeDir = SnakeDir.RIGHT
                pass
            elif event.key == game.K_p:
                self._sceneManager.push(
                    scenes.finishSceneName,
                    {"type": "pause"}
                )
                pass
        return super().handleEvent(event)

    def onBackground(self, surface: game.Surface, arg: Any) -> None:
        if self._everySecondInterval is not None:
            self._sceneManager.timing.removeInterval(self._everySecondInterval)
        return super().onBackground(surface, arg)
