from sizeConversion import CM
import scenes
from typing import Any, cast
from scene import Scene

import pygame as game
import pygame.event as event
import fonts

sceneName = "home"


COLOR = (0x00, 0x3F, 0x00)
MARGIN = CM(3)


class HomeScene(Scene):
    def __init__(self) -> None:
        super().__init__()

    def init(self, surface: game.Surface) -> None:
        return super().init(surface)

    def onOpen(self, surface: game.Surface, arg: Any) -> None:
        return super().onOpen(surface, arg)

    def logic(self, surface: game.Surface, dT: float) -> bool:
        return super().logic(surface, dT)

    def render(self, surface: game.Surface) -> None:
        size_x, size_y = surface.get_size()
        self._sceneManager.renderScene(scenes.snakeSceneName)
        newSurface = game.Surface(
            (size_x - MARGIN.toPx() * 2, size_y - MARGIN.toPx() * 2))
        surface_width, surface_height = newSurface.get_size()
        newSurface.set_alpha(0xAF)
        newSurface.fill(COLOR)
        if fonts.default_56 is not None:
            text: game.Surface = cast(game.Surface, fonts.default_56.render(
                "Press any key to start.",
                True,
                (0xFF, 0x00, 0x00)
            ))

        width, height = text.get_size()
        newSurface.blit(text, ((surface_width - width) //
                        2, (surface_height - height) // 2))
        surface.blit(newSurface, (MARGIN.toPx(), MARGIN.toPx()))
        return super().render(surface)

    def handleEvent(self, event: event.Event) -> None:
        if event.type == game.KEYDOWN:
            self._sceneManager.pushAndReplace(scenes.snakeSceneName)
        return super().handleEvent(event)

    def onBackground(self, surface: game.Surface, arg: Any) -> None:
        return super().onBackground(surface, arg)

    def onClose(self, surface: game.Surface, arg: Any) -> None:
        return super().onClose(surface, arg)
