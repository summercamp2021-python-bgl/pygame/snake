from __future__ import annotations

from typing import Any, Callable


class _Timer:
    _fn: Callable[[Any], None]
    _interval: int
    start: int

    def __init__(self, interval, start, fn) -> None:
        self._fn = fn  # type:ignore
        self._interval, self.start = interval, start
        pass


class Timing:
    _intervals: list[_Timer] = []
    _timeouts: list[_Timer] = []

    ms: int = 0

    def update(self, addMs: int) -> None:
        lastMs = self.ms
        self.ms += addMs

        for interval in self._intervals:
            if (lastMs - interval.start) // interval._interval < \
                    (self.ms - interval.start) // interval._interval:
                interval._fn()

        for timeout in self._timeouts:
            if (lastMs - timeout.start) // timeout._interval < \
                    (self.ms - timeout.start) // timeout._interval:
                timeout._fn()
                self._timeouts.remove(timeout)

    def setInterval(self, intervalMs: int, fn: Callable[[], None]):
        timer = _Timer(intervalMs, self.ms, fn)
        self._intervals.append(timer)
        return timer

    def setTimeout(self, intervalMs: int, fn: Callable[[], None]):
        timer = _Timer(intervalMs,  self.ms, fn)
        self._timeouts.append(timer)
        return timer

    def removeInterval(self, timer: _Timer):
        self._intervals.remove(timer)

    def removeTimeout(self, timer: _Timer):
        self._intervals.remove(timer)
