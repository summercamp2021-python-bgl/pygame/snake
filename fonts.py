from typing import Optional
import pygame.font as font

default_56: Optional[font.Font] = None
default_32: Optional[font.Font] = None


def init():
    global default_56
    default_56 = font.SysFont(None, 56)
    global default_32
    default_32 = font.SysFont(None, 32)
