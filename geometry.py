from sizeConversion import IMTupel
import pygame.draw as draw
import pygame as game


def setImPixel(surface: game.Surface, color: tuple[int, int, int],
               im: IMTupel, subPixel: tuple[int, int] = (0, 0)):
    surface.set_at(
        (im.x.toPx() + subPixel[0], im.y.toPx() + subPixel[1]), color)


def fillIMPixel(surface: game.Surface, color: tuple[int, int, int],
                im: IMTupel):
    rectVal = im.toRect()
    draw.rect(surface, color, game.Rect(
        rectVal[0], rectVal[1], rectVal[2], rectVal[3]))


def innerEllipseOfImPixel(surface: game.Surface, color: tuple[int, int, int],
                          im: IMTupel):
    rectVal = im.toRect()
    draw.ellipse(surface, color, game.Rect(
        rectVal[0], rectVal[1], rectVal[2], rectVal[3]))


def rect(surface: game.Surface, color: tuple[int, int, int], topLeft: IMTupel,
         size: IMTupel):
    topLeftPx = topLeft.toPx()
    sizePx = size.toPx()
    draw.rect(surface, color, game.Rect(
        topLeftPx[0], topLeftPx[1], sizePx[0], sizePx[1]))


def fillRegion(surface: game.Surface, color: tuple[int, int, int],
               topLeft: IMTupel, bottomRight: IMTupel):
    bottomRight.x.val -= topLeft.x.val
    bottomRight.y.val -= topLeft.y.val
    rect(surface, color, topLeft, bottomRight)


def ellipse(surface: game.Surface, color: tuple[int, int, int],
            topLeft: IMTupel, size: IMTupel):
    topLeftPx = topLeft.toPx()
    sizePx = size.toPx()
    draw.ellipse(surface, color, game.Rect(
        topLeftPx[0], topLeftPx[1], sizePx[0], sizePx[1]))


def ellipseInRegion(surface: game.Surface, color: tuple[int, int, int],
                    topLeft: IMTupel, bottomRight: IMTupel):
    bottomRight.x.val -= topLeft.x.val
    bottomRight.y.val -= topLeft.y.val
    ellipse(surface, color, topLeft, bottomRight)
