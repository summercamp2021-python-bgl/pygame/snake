from typing import cast
import pygame as game
import pygame.event as event
import pygame.display as display
import pygame.time as time

from scene import SceneManager
import scenes
import fonts

from constants import WINDOW_SIZE, WINDOW_TITLE, MAX_FPS

running: bool = True

sceneManager: SceneManager


def main() -> None:
    game.init()
    surface: game.Surface = cast(
        game.Surface, display.set_mode(WINDOW_SIZE.toPx()))
    display.set_caption(WINDOW_TITLE)
    clock = time.Clock()

    fonts.init()

    global sceneManager
    sceneManager = SceneManager([scenes.homeSceneName])
    sceneManager.register(scenes.homeSceneName, scenes.HomeScene())
    sceneManager.register(scenes.snakeSceneName, scenes.SnakeScene())
    sceneManager.register(scenes.finishSceneName, scenes.FinishScene())

    sceneManager.init(surface)

    frameTime = 0
    while running:
        for e in event.get():
            handle_event(e)
            pass
        display.flip()
        frameTime = clock.tick(MAX_FPS)
        sceneManager.render(surface, frameTime / 1000)
        pass

    game.quit()
    pass


def handle_event(event: event.Event) -> None:
    global running
    global sceneManager
    if event.type == game.QUIT:
        running = False
        pass
    sceneManager.handleEvent(event)
    pass


main()
