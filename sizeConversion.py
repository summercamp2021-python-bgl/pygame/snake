from __future__ import annotations

import abc


class IndependentMeassurements(metaclass=abc.ABCMeta):
    val: float
    @property
    @abc.abstractmethod
    def conversionRatio(self) -> float: pass

    def __init__(self, val: float) -> None:
        self.val = val
        pass

    @abc.abstractmethod
    def toPx(self) -> int: pass

    @classmethod
    @abc.abstractmethod
    def fromPx(cls, px: int) -> IndependentMeassurements: pass


DPI_CONVERSION_RATIO: int = 94


class IN(IndependentMeassurements):
    conversionRatio = DPI_CONVERSION_RATIO

    def toPx(self) -> int:
        return int(self.val * DPI_CONVERSION_RATIO // 1)

    @classmethod
    def fromPx(cls, px: int) -> IndependentMeassurements:
        return IN((px / DPI_CONVERSION_RATIO))


class CM(IndependentMeassurements):
    conversionRatio = DPI_CONVERSION_RATIO * 0.3937008

    def toPx(self) -> int:
        return int((self.val * self.conversionRatio) // 1)

    @classmethod
    def fromPx(cls, px: int) -> IndependentMeassurements:
        return CM((px / DPI_CONVERSION_RATIO * 0.3937008))


class IMTupel:
    x: IndependentMeassurements
    y: IndependentMeassurements

    def __init__(self, x: IndependentMeassurements,
                 y: IndependentMeassurements) -> None:
        self.x, self.y = x, y
        pass

    def toPx(self) -> tuple[int, int]:
        return (self.x.toPx(), self.y.toPx())

    def toRect(self) -> tuple[int, int, int, int]:
        return (
            self.x.toPx(),
            self.y.toPx(),
            int(self.x.conversionRatio // 1),
            int((self.y.conversionRatio) // 1)
        )

    def setX(self, val: IndependentMeassurements) -> IMTupel:
        self.x = val
        return self

    def setY(self, val: IndependentMeassurements) -> IMTupel:
        self.y = val
        return self

    def reassign(self, x: IndependentMeassurements,
                 y: IndependentMeassurements) -> IMTupel:
        self.x, self.y = x, y
        return self
